'use strict'

const __importDefault = (this && this.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { default: mod }
}
Object.defineProperty(exports, '__esModule', { value: true })
const expressUno = __importDefault(require('express'))
const dotenvUno = __importDefault(require('dotenv'))
// Configuration the .env file
dotenvUno.default.config()
// create Express App
const app = (0, expressUno.default)()
const port = process.env.PORT || 8000
/**
 * @DefinethefirstApproute
 * **/
app.get('/', (req, res) => {
// Send Hello World
  res.send('Welcome to Api Restful: Express + Nodemon + Jest + TS + Swagger + Mongoose')
})
app.listen(port, () => `Running at the port ${port}`)
// # sourceMappingURL=index.js.map

import express, { Express,Request,Response } from "express";
import dotenv from 'dotenv';

// Configuration the .env file
dotenv.config()
// create Express App
const app: Express = express();
const port:any = process.env.PORT || 8000;

/**
 * @DefinethefirstApproute
 * **/
app.get('/',(req: Request,res: Response)=>{
    // Send Hello World
    res.send('Welcome to Api Restful: Express + Nodemon + Jest + TS + Swagger + Mongoose')
});
app.listen(port, ()=>{`Running at the port ${port}`})